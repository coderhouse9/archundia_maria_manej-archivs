const Contenedor = require("./Contenedor");

const contenedor = new Contenedor("personajes.json");

const main = async () => {
    const id1 = await contenedor.save({
        "species": "Saiyan",
        "status": "Alive",
        "originPlanet": "Earth",
        "gender": "Male",
        "name": "Gohan",
    });
    const id2 = await contenedor.save({
        "species": "Namekiano",
        "status": "Alive",
        "originPlanet": "Namek",
        "gender": "Male",
        "name": "Piccolo",
    });


    console.log(id1, id2); // 1, 2

    const object2 = await contenedor.getById(2);
    console.log(object2);

    await contenedor.deleteById(2);

    const allCurrentObjects = await contenedor.getAll();
    console.log(allCurrentObjects);

};

main();